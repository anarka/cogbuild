#include <CogCore/App.h>
#include <CogBuild/ClassWriter.h>

CogMain
  (void)args;
  return Compile::JITCog{Filesystem::File{"Test.Cog"}}.Cog().GetFunction<int(*)()>("TestFunc")();
EndMain
