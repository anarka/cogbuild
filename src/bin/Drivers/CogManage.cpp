#include <cstring>
#include <string>
#include <CogCore/Directory.h>
#include <CogBuild/Workspace.h>
#include <CogBuild/Installed.h>
#include <CogCore/Logger.h>
#include <CogCore/App.h>

CogMain
  if(args.size() < 1)
  {
    Logging::ErrorLog("No argument provided");
    return -1;
  }

  if(args[0] == "init")
  {
    if (args.size() < 2)
    {
      Logging::ErrorLog("Error, init requires new project name!");
      return 1;
    }

    Logging::ColorLog("Creating project %s", Logging::Colors::GREEN, args[1]);
    Project::Workspace::Create(Filesystem::File{std::string{args[1]}});
  }
  else if(args[0] == "install")
  {
    auto project = Filesystem::Directory::WorkingDir();
    Logging::ColorLog("Starting install for project %s", Logging::Colors::GREEN, project);
    Project::Installed{std::optional(std::string{"Release"})};
  }
EndMain
