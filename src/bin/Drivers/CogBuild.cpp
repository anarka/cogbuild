//TODO: Prune these
#include <CogCore/Logger.h>
#include <CogCore/Directory.h>
#include <CogBuild/Static.h>
#include <CogBuild/Binary.h>
#include <CogBuild/Compiler.h>
#include <CogCore/Process.h>
#include <CogBuild/Built.h>
#include <CogCore/App.h>

#include <cstring>
#include <iostream>

CogMain
  bool handled{false};
  if(args.size() > 0)
  {
    if(args[0] == "bootstrap" || args[0] == "rebuild")
    {
      std::filesystem::remove_all("build");
      handled = true;
    }
    else if(args[0] == "clean")
    {
      Logging::ColorLog("Build dir cleaned!", Logging::Colors::GREEN);
      std::filesystem::remove_all("build");
      return 0;
    }
  }
  
  const size_t pos = handled ? 1 : 0;
  const auto projectDir = args.size() > pos + 1 ? std::optional(std::string{args[pos]}) : std::nullopt;
  const auto built = Project::Built{projectDir};

  Logging::ColorLog("Build Successful!", Cog::Logging::Colors::GREEN);
EndMain 
