#include <chrono>
#include <thread>
#include <CogCore/Process.h>
#include <CogCore/File.h>
#include <CogCore/Directory.h>

int main(int argc, char** args)
{
  Cog::Filesystem::Directory::EnsureExists(".buildserver/");

  Cog::Filesystem::File rebuildFile{".buildserver/rebuild"};
  Cog::Filesystem::File shutdownFile{".buildserver/shutdown"};
  Cog::Filesystem::File highlanderFile{".buildserver/highlander"};

  std::vector<std::string> arguments{};

  if (argc > 1)
  {
    int argi{};
    while(argi < argc)
    {
      if(strcmp(args[argi], "rebuild") == 0)
      {
        rebuildFile.Create(); 
      }
      else if(strcmp(args[argi], "shutdown") == 0)
      {
        shutdownFile.Create();
        return 0;
      }
    }
  }

  if (!highlanderFile.Exists())
  {
    highlanderFile.Create();
    
    while(true)
    {
      if (shutdownFile.Exists())
      {
        shutdownFile.Delete();
        rebuildFile.Delete();
        highlanderFile.Delete();
        return 0;
      }   
      else if (rebuildFile.Exists())
      {
        rebuildFile.Delete();
        arguments.push_back("rebuild");
      }
      
      Cog::System::Process::CreateAndWait("CogBuild", arguments);
      arguments.clear();
      
      std::this_thread::sleep_for(std::chrono::seconds{15});
    }
  }
}

