#include "Installed.h"
#include <CogCore/Directory.h>

namespace Cog::Project
{

  const Filesystem::RequiredDirectory Installed::Cog_BIN{Loaded::Cog_BIN_DIR};
  const Filesystem::RequiredDirectory Installed::Cog_LIB{Loaded::Cog_LIB_DIR};
  const Filesystem::RequiredDirectory Installed::Cog_INCLUDE{Loaded::Cog_INCLUDE_DIR};
  
  Installed::Installed(std::optional<std::string> buildConfig)
    : Built{buildConfig},
      Loaded{Name(), true}
  {
    Filesystem::Directory::EnsureExists(Loaded::m_driversDir);

    auto installAndReferenceFrom = [](const auto& source, const auto& target, const auto& refDir)
    {
      source.EnsureCopyTo(target);
      target.EnsureSymlinkTo(source.LocatedAt(refDir));
    };

    auto cogFiles = Filesystem::ExistingDirectory{Filesystem::Directory::WorkingDir()}.CollectFilesByExt(".cog");

    for (auto&& cogFile : cogFiles)
    {
      auto target = cogFile.LocatedAt(Loaded::m_buildCog.Location().Path());
      cogFile.DoIfNewer(target, &Filesystem::File::EnsureCopyTo, cogFile, target);
    }

    auto cogFileDir = Filesystem::File{".cog"};

    if(cogFileDir.Exists())
    {
      auto cogFileDirectory = Filesystem::ExistingDirectory{cogFileDir.Path()};

      cogFileDirectory.ForEveryEntry([this](auto&& entry)
      {
        auto target = entry.BasedAt(m_buildCog.Location().Path() + "/");
        entry.DoIfNewer(target, &Filesystem::File::EnsureCopyTo, entry, target);
      });
    }

    for(auto&& [language, buildArtifacts] : m_artifacts)
    {
      auto& currentLanguage = m_languageArtifacts[language];

      for(auto&& driver : buildArtifacts.m_drivers.Binaries())
      {
        auto targetDir = driver.LocatedAt(Loaded::m_driversDir.Path());
        Logging::ColorLog("Installing driver %s", Logging::Colors::MAGENTA, driver.Name());

        driver.DoIfNewer(targetDir, installAndReferenceFrom, driver, targetDir, Cog_BIN_DIR);
      }

      Logging::ColorLog("Installing library %s, interface to %s", Logging::Colors::MAGENTA, buildArtifacts.m_lib.Library().Name(), currentLanguage.m_includeDir);

      Filesystem::Directory::EnsureExists(currentLanguage.m_includeDir);
      for(auto&& include : buildArtifacts.m_lib.Includes())
      {
        auto targetFile = include.LocatedAt(currentLanguage.m_includeDir.Path());
        include.DoIfNewer(targetFile, &Filesystem::File::EnsureCopyTo, include, targetFile); 
      }

      auto archiveFile = buildArtifacts.m_lib.Library();
    
      archiveFile.DoIfNewer(currentLanguage.m_archive, installAndReferenceFrom, archiveFile, currentLanguage.m_archive, Cog_LIB_DIR);

      Logging::DebugLog("%s", currentLanguage.m_includeDir.Location().Location());

      if(!Dependable(language))
      {
        throw std::runtime_error("Install failed to make project usable as a dependency!");
      }
    }

    Filesystem::File{m_buildCog.Location().Path() + "/include/"}.EnsureSymlinkTo(Filesystem::File{Cog_INCLUDE_DIR + Name()});
  }
}
