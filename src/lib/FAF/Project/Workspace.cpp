#include "Workspace.h"

namespace Cog::Project
{ 
  Workspace Workspace::Create(const Filesystem::File& projectName)
  {
    Workspace newProject{};

    if(newProject.IsConstructed())
    {
      throw std::runtime_error("Project already created!");
    }

    Filesystem::Directory::SetupNew(projectName, [&newProject, project=projectName.Name()]()
    {
      Logging::ColorLog("Setting up source dirs", Logging::Colors::GREEN);

      Filesystem::File::CreateNew(".gitignore", "#Cog default ignores\nbuild/\n");
      Filesystem::Directory::SetupNew(Filesystem::File{".cog"}, [&project]()
      {
        Filesystem::File::CreateNew("Build", "ProjectName=" + project + "\nLanguages=c++\nDependencies=CogCore");
        Filesystem::Directory::EnsureExists(".BuildConfig/.c++");
        Filesystem::Directory::EnsureExists(".Languages");
        Filesystem::File::CreateNew(".Languages/c++", "Compiler=g++-10\nSourceExt=.cpp\nInterfaceExt=.h\nFlags=stdc++=2a,Wall,Wextra,Werror");
        Filesystem::File::CreateNew(".BuildConfig/.c++/Debug", "Flags=O0,g");
        Filesystem::File::CreateNew(".BuildConfig/.c++/Release", "Flags=O3,flto,DNO_FROM_LOGGING,DNDEBUG");
      });

      Filesystem::File::CreateNew("README.md", "# " + project + "\nA Cog generated project\n");
      Filesystem::Directory::WorkIn(newProject.m_driversDir, Filesystem::File::CreateNew, "/README.md", "#Drivers\nAll driver classes will be generated in this dir\n");
      Filesystem::Directory::WorkIn(newProject.m_libsDir, Filesystem::File::CreateNew, "/README.md", "#Lib\nProgram logic classes go here, these will build into static libs\n");
      Filesystem::Directory::WorkIn(newProject.m_testsDir, Filesystem::File::CreateNew, "/README.md", "#Tests\nEvery class needs tests, which will be run before drivers build\n");

      Logging::ColorLog("Creating git repo", Logging::Colors::GREEN);
      System::Process::CreateAndWait("git", {"init"});

      Logging::ColorLog("Adding project to repo", Logging::Colors::GREEN);
      System::Process::CreateAndWait("git", {"add", "."});

      Logging::ColorLog("Commiting project to repo", Logging::Colors::GREEN);
      System::Process::CreateAndWait("git", {"-c","user.name=Cogmanage", "-c", "user.email=Cogmanage@auto.noemail", "commit", "-m\"Cog Init\""});

      if(!newProject.IsConstructed())
      {
        throw std::runtime_error("An unspecified error prevented project construction! Project is left in place as this may be a false positive.");
      }
    });



    return newProject;
  } 

  bool Workspace::IsConstructed()
  {
    return m_libsDir.Exists()
	      && m_testsDir.Exists()
	      && Filesystem::File(".git").Exists();
  }
}
