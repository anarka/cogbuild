#pragma once
#include <string>
#include <CogCore/RequiredDirectory.h>
#include <CogBuild/Loaded.h>
#include <CogBuild/Built.h>

namespace Cog::Project
{

  class Installed : public Built, public Loaded
  {
  public: 
    static const Filesystem::RequiredDirectory Cog_BIN;
    static const Filesystem::RequiredDirectory Cog_INCLUDE;
    static const Filesystem::RequiredDirectory Cog_LIB;

    Installed(std::optional<std::string> = std::nullopt);
  };
}
