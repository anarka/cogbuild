#pragma once
#include <string>
#include <set>
#include <CogCore/File.h>
#include <CogBuild/BuildCog.h>
#include <CogBuild/Compiler.h>
#include <CogCore/Logger.h>

namespace Cog::Project
{
  class Loaded
  {
  public: 
    
    class Hashor
    {
    public:
      int operator()(const Loaded& toHash) const
      {
        return std::hash<std::string>()(toHash.m_projectName);
      }
    };
    
    using DependencyMap = std::unordered_map<Loaded, std::reference_wrapper<const Build::BuildCog>, Hashor>;
    
    static constexpr auto INSTALL_BASE_DIR = "Cog/";
    static const std::string INSTALL_DIR;
    static const std::string Cog_BIN_DIR;
    static const std::string Cog_LIB_DIR;
    static const std::string Cog_INCLUDE_DIR;

    Loaded(const std::string& loaded, bool autoLoad = false);

    DependencyMap ResolveDependencies() const;

    bool Dependable(const std::string& language) const
    {
      return HasLanguage(language) && m_languageArtifacts.at(language).m_includeDir.Exists();
    }

    const Filesystem::File& Archive(const std::string& language) const
    {
      if(HasLanguage(language))
      {
        return m_languageArtifacts.at(language).m_archive;
      }
      else
      {
        throw "Language not present";
      }
    }

    const Filesystem::File& IncludeDir(const std::string& language) const
    {
      if(HasLanguage(language))
      {
        return m_languageArtifacts.at(language).m_includeDir;
      }
      else
      {
        throw "Language not present";
      }
    }

    bool HasLanguage(const std::string& language) const
    {
      return m_languageArtifacts.count(language) != 0;
    }

    bool operator==(const Loaded& rhs) const
    {
      return m_languageArtifacts == rhs.m_languageArtifacts; 
    }

    static DependencyMap DependenciesFrom(const Filesystem::File& toLoad);

    static Filesystem::File GetInstalledDir(const std::string& project, const std::string& dir = "/");
  protected: 
    DependencyMap Dependencies(const Filesystem::File& configFile) const;

    struct InstalledLanguage
    {
      Filesystem::File m_archive;
      Filesystem::File m_includeDir;
      bool operator==(const InstalledLanguage& rhs) const
      {
        return m_archive == rhs.m_archive && m_includeDir == rhs.m_includeDir;
      }
    };


    std::string m_projectName;
    Filesystem::File m_driversDir;
    Filesystem::File m_buildCog; 
    std::unordered_map<std::string, InstalledLanguage> m_languageArtifacts{};
  };
}
