#include <CogCore/File.h>
#include <CogCore/ParallelFor.h>
#include <CogCore/Vector.h>
#include <CogBuild/Static.h>
#include <CogBuild/Binary.h>

#include <CogBuild/Loaded.h>
#include <CogBuild/Built.h>
#include <CogBuild/ProjectFile.h>

namespace Cog::Project
{
  Built::Built(std::optional<std::string> buildConfig)
    : Workspace{}
  { 
    if(!IsConstructed())
    {
      throw std::runtime_error("Can't build malformed project!");
    }

    if(buildConfig)
    {
      ConfigFile().BuildConfig(*buildConfig);
    }

    const auto& configFile = ConfigFile().Cogfile();

    for (auto& languageCog : configFile.Languages())
    {
      auto& curLanguage = m_artifacts[languageCog->Namespace()];
      try
      {
        {
          auto libDir = Cog::Filesystem::ExistingDirectory(m_libsDir.Path());
          Cog::Logging::ColorLog("Starting library %s build!", Cog::Logging::Colors::MAGENTA, Name());
          curLanguage.m_lib = Cog::Build::StaticLibrary::Build(libDir, *languageCog);
          Cog::Logging::ColorLog("Library built!", Cog::Logging::Colors::GREEN);
        }
      }
      catch(std::exception& e)
      {
        Cog::Logging::ErrorLog("Libraries failed to build");
        throw;
      }

      try
      {
        auto testDir = Filesystem::ExistingDirectory(m_testsDir.Path());
        Cog::Logging::ColorLog("Starting tests build!", Cog::Logging::Colors::GREEN);
        curLanguage.m_tests = Cog::Build::Binary::Build(testDir, {curLanguage.m_lib}, *languageCog);
     
        Cog::Logging::ColorLog("Tests built! Now running!", Cog::Logging::Colors::GREEN);
      
        RunTests(curLanguage);
        
        Cog::Logging::ColorLog("Tests passed!", Cog::Logging::Colors::GREEN);
      }
      catch(std::exception& e)
      {
        Cog::Logging::ErrorLog("Test phase failed!");

        throw;
      }

      if(!IsDriverless())
      {
        try
        {
          auto binDir = Filesystem::ExistingDirectory(m_driversDir.Path());
          Cog::Logging::ColorLog("Starting binary build!", Logging::Colors::GREEN);
          curLanguage.m_drivers = Build::Binary::Build(binDir, {curLanguage.m_lib}, *languageCog);
          Logging::ColorLog("Binaries built!", Logging::Colors::GREEN);
        }
        catch(std::exception& e)
        {
          Cog::Logging::ErrorLog("Binaries failed to build!");

          throw;
        }
      }
    }
      Cog::Logging::ColorLog("Build completed!", Cog::Logging::Colors::GREEN);  
  }

  void Built::RunTests(const BuiltLanguage& curLanguage)
  {
    Async::ParallelDispatcher<void> RunTests{curLanguage.m_tests.Binaries(), [](auto&& test)
    {
      auto&& testName = test.Name();
      Logging::ColorLog("Running test %s", Logging::Colors::GREEN, testName);
      auto runningTest = System::Process(test.Path());

      if(runningTest.ReturnValue() == 0)
      {
        Logging::ColorLog("Test %s passed!", Logging::Colors::GREEN, testName);
      }
      else
      {
        Logging::ErrorLog("Test %s failed!", testName);
      }
    }};

  }

}
