#pragma once
#include <CogCore/ConfigFile.h>
#include <CogBuild/BuildCog.h>
#include <CogBuild/Loaded.h>

namespace Cog::Project
{
  COG(Project)
    NEW_COG_VARIABLE(Cogfile, &Build::BuildCog::Get())
      CONVERT_FUNCTION()
      {
        return &Build::BuildCog::Get(Filesystem::File{inVal});
      }

      MY_FINALIZE_FUNCTION()
      {
        const auto depends = Loaded(toFinalize->ProjectName()).ResolveDependencies(); 

        auto&& buildConfig = My.BuildConfig();

        for(auto& languageCog : toFinalize->Languages())
        {
          if (buildConfig)
          {
            languageCog->BuildConfig(*buildConfig);
          }

          auto& compiler = const_cast<Build::Compiler&>(languageCog->Compiler());
          auto&& language = languageCog->Namespace();

          for(const auto& [depend, configFile] : depends)
          {
            if(!depend.HasLanguage(language))
            {
              continue;
            }

            if (depend.Archive(language).Exists())
            {
              compiler.AddStaticLib(depend.Archive(language));
            }

            compiler.AddIncludeDir(Filesystem::File{Loaded::Cog_INCLUDE_DIR + configFile.get().ProjectName() + "/" + language + "/"});

            for(auto&& languageCog : configFile.get().Languages())
            {
              if(languageCog->Namespace() == language)
              {
                for(const auto& lib : languageCog->ExternalLibs())
                {
                  compiler.AddLinkerDepends(lib);
                }
              }
            }
          }

          compiler.AddIncludeDir(languageCog->LocalIncludeDir().Location().Location());
        }
      }
    END_COG_VARIABLE(Cogfile)

    NEW_COG_VARIABLE(BuildConfig, std::optional<std::string>{std::nullopt})
      OVERRIDEABLE
      CONVERT_FUNCTION()
      {
        return std::optional(std::string{inVal});
      }
    END_COG_VARIABLE(BuildConfig)
  END_COG()
}
