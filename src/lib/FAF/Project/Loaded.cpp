#include <CogCore/XDGPaths.h>

#include <CogBuild/Loaded.h>


namespace Cog::Project
{
  const std::string Loaded::INSTALL_DIR = Filesystem::XDGPaths::Variables().GetDataHome() + INSTALL_BASE_DIR;
  const std::string Loaded::Cog_BIN_DIR = Loaded::INSTALL_DIR + "bin/";
  const std::string Loaded::Cog_LIB_DIR = Loaded::INSTALL_DIR + "lib/";
  const std::string Loaded::Cog_INCLUDE_DIR = Loaded::INSTALL_DIR + "include/"; 

  Loaded::Loaded(const std::string& projectName, bool autoLoad)
    : m_projectName{projectName},
      m_driversDir{GetInstalledDir(projectName, "/Drivers/")},
      m_buildCog{GetInstalledDir(projectName, "/")}
  {
    if (autoLoad)
    {
      auto& buildCog = Build::BuildCog::Get(m_buildCog);

      for(auto& languageCog : buildCog.Languages())
      {
        auto& upstreamLanguage = m_languageArtifacts[languageCog->Namespace()];
        upstreamLanguage.m_archive = GetInstalledDir(m_projectName, "/lib/" + languageCog->ArchiveFile().Fullname());
        upstreamLanguage.m_includeDir = GetInstalledDir(m_projectName, "/include/" + languageCog->Namespace() + "/" + projectName + "/");
      }
    }
  }

  Loaded::DependencyMap Loaded::ResolveDependencies() const
  {
    auto thisDepends = DependenciesFrom(Filesystem::File{"./"});

    // TODO: Fix this monstrosity
    for(auto& [dependency, configFile] : thisDepends)
    {
      const auto dependDepends = DependenciesFrom(dependency.m_buildCog);

      for(auto& furtherDependency : dependDepends)
      {
        if(!thisDepends.count(furtherDependency.first))
        {
          thisDepends.emplace(std::move(furtherDependency));
        }
      }
    }

    return thisDepends;
  }

  Loaded::DependencyMap Loaded::DependenciesFrom(const Filesystem::File& configFile)
  {
    const Build::BuildCog& toLoad = Build::BuildCog::Get(configFile);

    DependencyMap out{};
    const auto startingSet = toLoad.Dependencies();

    for(auto&& project : startingSet)
    {
      const auto loadedProj = Loaded{project, true};
      const auto projConfig = std::ref(Build::BuildCog::Get(loadedProj.m_buildCog));
      out.emplace(std::piecewise_construct, std::forward_as_tuple(loadedProj), std::forward_as_tuple(projConfig));
    }

    return out;
  }

  Filesystem::File Loaded::GetInstalledDir(const std::string& project, const std::string& dir)
  {
    return Filesystem::File{INSTALL_DIR + project + dir};
  }
}
