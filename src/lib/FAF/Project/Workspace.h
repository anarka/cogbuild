#pragma once

#include <string>
#include <CogCore/File.h>
#include <CogCore/Directory.h>
#include <CogCore/XDGPaths.h>
#include <CogBuild/Compiler.h>
#include <CogBuild/Binary.h>
#include <CogBuild/ProjectFile.h>
#include <CogCore/Process.h>
#include <CogCore/Logger.h>

namespace Cog::Project
{
  class Workspace
  {
  public:
    static constexpr auto LIB_DIR = "src/lib";
    static constexpr auto DRIVER_DIR = "src/bin/Drivers";
    static constexpr auto TESTS_DIR = "src/bin/Tests";

    Workspace()
    {
    };

    ~Workspace(){}

    std::string Name() const
    {
      return ConfigFile().Cogfile().ProjectName();
    }

    static Workspace Create(const Filesystem::File& projectName);

    bool IsDriverless()
    {
      return !m_driversDir.Exists();
    }

    const ProjectCog& ConfigFile() const
    {
      return m_config;
    }

  protected:
    const ProjectCog& m_config{ProjectCog::Get()};
    Filesystem::File m_libsDir{LIB_DIR};
    Filesystem::File m_driversDir{DRIVER_DIR};
    Filesystem::File m_testsDir{TESTS_DIR};

    bool IsConstructed();
  };
}
