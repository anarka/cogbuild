#pragma once
#include <CogBuild/Binary.h>
#include <CogBuild/Static.h>
#include <CogBuild/Workspace.h>

namespace Cog::Project
{
  class Built : public Workspace
  {
  public:
    Built(std::optional<std::string> = std::nullopt);

    std::optional<Filesystem::File> FindDriver(const std::string_view& toFind) const
    {
      std::optional<Filesystem::File> out{};
      
      for(auto&& [language, buildArtifacts] : m_artifacts)
      {
        if (out)
        {
          break;
        }

        for(auto& driver : buildArtifacts.m_drivers.Binaries())
        {
          if(toFind == driver.Fullname())
          {
            out.emplace(driver);
            break;
          }
        }
      }

      return out;
    }

  protected:
    struct BuiltLanguage
    {
      Build::Binary m_tests{};
      Build::Binary m_drivers{};
      Build::StaticLibrary m_lib;
    };

    std::unordered_map<std::string, BuiltLanguage> m_artifacts{}; 
  
    void RunTests(const BuiltLanguage&);
  };

}
