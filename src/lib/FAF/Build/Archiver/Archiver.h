#pragma once
#include <CogCore/Logger.h>
#include <CogCore/File.h>
#include <CogCore/Process.h>
#include <mutex>

namespace Cog::Build
{
  
  class Archiver
  {
  public:
    Archiver(const Filesystem::File& archive) :
      m_archive{archive},
      m_startTime{m_archive.Timestamp()}
    {}
 
    const Filesystem::File& Archive(const Filesystem::File& objectFile) const;

  private:
    const Filesystem::File& m_archive;
    const std::filesystem::file_time_type m_startTime{};
    mutable std::mutex m_processLock{};
  };

}
