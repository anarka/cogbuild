#pragma once

#include <CogCore/Directory.h>
#include <CogCore/File.h>
#include <CogCore/Vector.h>

#include <CogBuild/Static.h>
#include <CogBuild/BuildCog.h>

namespace Cog::Build
{

  class Binary
  {
  public:
    static Binary Build(const Filesystem::ExistingDirectory& libDir, const std::vector<StaticLibrary>& depends, const LanguageCog<BuildCog>& configFile);
    std::string Catagory() const;

    ~Binary(){}

    const Collections::Vector<Filesystem::File> Binaries() const
    {
      return m_binaries;
    }

    Binary() = default;
  private:

    std::string m_catagory{};
    Collections::Vector<Filesystem::File> m_binaries{};
  };
}
