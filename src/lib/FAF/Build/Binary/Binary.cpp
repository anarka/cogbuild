#include <CogBuild/Static.h>
#include <CogBuild/BuildCog.h>
#include <CogCore/Process.h>
#include <CogCore/ParallelFor.h>

#include <CogBuild/Binary.h>

namespace Cog::Build
{
  Binary Binary::Build(const Filesystem::ExistingDirectory& binDir, const std::vector<StaticLibrary>& depends, const LanguageCog<BuildCog>& languageCog)
  {
    Binary newBin{};
  
    newBin.m_catagory = binDir.AsFile().Fullname();

    Logging::ColorLog("Building catagory %s", Logging::Colors::GREEN, newBin.m_catagory);
    
    auto compiler = languageCog.Compiler();
    
    for(auto&& lib : depends)
    {
      if(lib.Library().Exists())
      {
        compiler.AddStaticLib(lib.Library());
      }    
    }

    const auto drivers = binDir.CollectFilesByExt(languageCog.SourceExt());

    Async::ParallelDispatcher<Filesystem::File>{drivers, Functional::OnThis(&Compiler::Compile, compiler), true}
      .Wait("Failed to build driver!", Functional::OnThis(&Collections::Vector<Filesystem::File>::EmplaceBack<const Filesystem::File&>, newBin.m_binaries));

    Logging::ColorLog("Done building catagory %s", Logging::Colors::GREEN, newBin.m_catagory);
    
    return newBin;
  }


  std::string Binary::Catagory() const
  {
    return m_catagory;
  }
}
