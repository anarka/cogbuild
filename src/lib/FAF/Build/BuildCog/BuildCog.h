#pragma once

#include <string>
#include <unordered_set>
#include <unordered_map>
#include <utility>
#include <functional>
#include <CogCore/ConfigFile.h>
#include <CogCore/Logger.h>
#include <CogCore/File.h>
#include <CogCore/Directory.h>
#include <CogCore/RequiredDirectory.h>
#include <CogBuild/Compiler.h>
#include <CogBuild/ClassWriter.h>

namespace Cog::Build
{
  class PushBackSet
  {
  public:
    PushBackSet() = default;
    PushBackSet(std::string_view inStr) :
      myTarget{inStr.empty() ? std::vector<std::string>{} : Tokenize(inStr, ',')}
    {}

    void Set(const std::string_view& toSet)
    {
      auto&& temp = Tokenize(toSet, ',');
      for(auto&&val : temp)
      {
        myTarget.emplace_back(std::move(val));
      }
    }

    const auto& operator()() const
    {
      return myTarget;
    }

  private:
    std::vector<std::string> myTarget{};
  };

  NAMESPACED_SUBCOG(BuildConfig)
    NEW_COG_SET(Flags, std::string)
      MY_KEY_CONVERT_FUNCTION()
      {
        return My.Parent().CompilerFlagToken() + std::string{toConvert};
      }
    END_COG_VARIABLE(Flags)
  END_COG()

  NAMESPACED_SUBCOG(Toolchain) 
    NEW_COG_MAP(DynamicArgs, std::string, PushBackSet)
      MY_MAP_FUNCTION()
      { 
        auto&& entry = map.emplace(key, value);
        My.Register(entry.first->first, entry.first->second);
      } 
    END_COG_VARIABLE(DynamicArgs)

    void DebugPrint() const
    {
      for(auto&& [argName, entries] : DynamicArgs())
      {
        auto&& logger = Logging::ColorLog("Dynamic Variable %s", Logging::Colors::YELLOW, argName);
        for(auto&& entry : entries())
        {
          logger.Log("%s", entry);
        }
      }
    }
   
    void Execute() const 
    {
      using FuncType = void(*)(const CurrentFile&, const std::string&);
      return this->JitFunctions()->Cog().template GetFunction<FuncType>("Execute")(*this, "Bonus");
    } 

    NEW_COG_VARIABLE(Compiler, Build::Compiler{})
/*      MY_FINALIZE_FUNCTION()
      {
        for(auto&& flag : My.Flags())
        {
          toFinalize.AddFlag(flag);
        }

        for(auto&& lib : My.ExternalLibs())
        {
          toFinalize.AddLinkerDepends(lib);
        }

        toFinalize.SetBinaryOutputDir(My.LocalDriversDir());
        toFinalize.SetArchiveOutputDir(My.LocalArchiveDir());
      }
  */  END_COG_VARIABLE(Compiler)
   
    NEW_COG_DERIVED(JitFunctions, std::optional<Compile::JITCog>)
    {
      toFinalize.emplace(My.LoadedPath(), My.Parent().LocalArchiveDir());
    }
    END_PRIVATE_COG_VARIABLE(JitFunctions)


  END_COG()

  NAMESPACED_SUBCOG(Language)
    NEW_COG_VARIABLE(Compiler, Build::Compiler{})
      MY_FINALIZE_FUNCTION()
      {
        for(auto&& flag : My.Flags())
        {
          toFinalize.AddFlag(flag);
        }

        for(auto&& lib : My.ExternalLibs())
        {
          toFinalize.AddLinkerDepends(lib);
        }

        toFinalize.SetBinaryOutputDir(My.LocalDriversDir());
        toFinalize.SetArchiveOutputDir(My.LocalArchiveDir());

        My.TestToolchain().DebugPrint();
        My.TestToolchain().Execute();
      }
    END_COG_VARIABLE(Compiler)
    
    NEW_COG_SIMPLE(SourceExt, "")

    NEW_COG_SIMPLE(InterfaceExt, "NO_EXT")

    NEW_COG_SIMPLE(LibraryPrefix, "lib")

    NEW_COG_SIMPLE(LibraryExt, ".a")
    
    NEW_COG_SIMPLE(CompilerFlagToken, "-")

    NEW_COG_SIMPLE(CompilerLinkerPrefix, "l")

    NEW_COG_SET(Flags, std::string)
      MY_FINALIZE_FUNCTION()
      {
        for(auto&& flag : My.BuildConfigExtra().Flags())
        {
          toFinalize.emplace(flag);
        }
      }

      MY_KEY_CONVERT_FUNCTION()
      {
        return My.CompilerFlagToken() + std::string{toConvert};
      }
    END_COG_VARIABLE(Flags)

    NEW_COG_DERIVED(ArchiveName, std::string)
    {
      toFinalize = My.LibraryPrefix() + My.Parent().ProjectName() + My.LibraryExt();
    }
    END_COG_VARIABLE(ArchiveName)
    
    NEW_COG_DERIVED(ArchiveFile, Filesystem::File)
    {
      toFinalize = Filesystem::File{My.ArchiveName()}.WithExt(My.LibraryExt()).LocatedAt(My.Parent().LibraryDir());
    }
    END_COG_VARIABLE(ArchiveFile)
    
    NEW_COG_DERIVED(LocalIncludeDir, Filesystem::RequiredDirectory)
    {
      toFinalize = My.Parent().BuildRoot() + My.Parent().IncludeDir() + My.Namespace() + "/" + My.Parent().ProjectName() + "/";
    }
    END_COG_VARIABLE(LocalIncludeDir)
    
    NEW_COG_SET(ExternalLibs, std::string)
      MY_KEY_CONVERT_FUNCTION()
      {
        return My.CompilerFlagToken() + My.CompilerLinkerPrefix() + std::string{toConvert};
      }
    END_COG_VARIABLE(ExternalLibs)
    
    NEW_COG_DERIVED(LocalDriversDir, Filesystem::RequiredDirectory)
    {
      toFinalize = My.Parent().BuildRoot() + My.Parent().DriversDir() + My.Namespace() + "/" + My.BuildConfig() + "/";
    }
    END_COG_VARIABLE(LocalDriversDir)
    
    NEW_COG_DERIVED(LocalArchiveDir, Filesystem::RequiredDirectory)
    {
      toFinalize = My.Parent().BuildRoot() + My.Parent().ArchiveDir() + My.Namespace() + "/" + My.BuildConfig() + "/";
    }
    END_COG_VARIABLE(LocalArchiveDir)

    NEW_COG_VARIABLE(BuildConfig, std::string{"Debug"})
      OVERRIDEABLE
      
      MY_APPEND_FUNCTION()
      {
        toAppend = std::string{inVal};
        My.BuildConfigExtra.Finalize();
      } 
    END_COG_VARIABLE(BuildConfig)

    NEW_COG_DERIVED(BuildConfigExtra, const BuildConfigCog<CurrentFile>*)
    {
      toFinalize = &BuildConfigCog<CurrentFile>::Get(My.BuildConfig() + "." + My.Namespace(), My, Filesystem::File{My.Location().Path() + "/"});
    }
    END_PRIVATE_COG_VARIABLE(BuildConfigExtra)
    
    NEW_COG_DERIVED(TestToolchain, const ToolchainCog<CurrentFile>*)
    {
      toFinalize = &ToolchainCog<CurrentFile>::Get(My.BuildConfig() + "." + My.Namespace(), My, Filesystem::File{My.Location().Path() + "/"});
    }
    END_PRIVATE_COG_VARIABLE(TestToolchain)
  END_COG()

  COG(Build) 
    NEW_COG_SET(Languages, const LanguageCog<BuildCog>*)
      MY_KEY_CONVERT_FUNCTION()
      {
        return &LanguageCog<BuildCog>::Get(std::string{toConvert}, My, Filesystem::File{My.Location().Path() + "/"});
      }
    END_COG_VARIABLE(Languages)

    NEW_COG_SIMPLE(ProjectName, "DefaultProject")
    
    NEW_COG_SIMPLE(LibraryPrefix, "lib")

    NEW_COG_SIMPLE(LibraryExt, ".a")

    COG_MAGIC_VALUE(LibDir, std::string{"lib/"})

    NEW_COG_DERIVED(LibraryDir, Filesystem::RequiredDirectory)
    {
      toFinalize = My.BuildRoot() + My.LibDir(); 
    }
    END_COG_VARIABLE(LibraryDir)
  
    NEW_COG_DERIVED(ArchiveName, std::string)
    {
      toFinalize = My.LibraryPrefix() + My.ProjectName() + My.LibraryExt();
    }
    END_COG_VARIABLE(ArchiveName)

    NEW_COG_DERIVED(ArchiveFile, Filesystem::File)
    {
      toFinalize = Filesystem::File{My.ArchiveName()}.WithExt(My.LibraryExt()).LocatedAt(My.LibraryDir());
    }
    END_COG_VARIABLE(ArchiveFile)
    
    COG_MAGIC_VALUE(BuildRoot, std::string{"build/"})
    
    COG_MAGIC_VALUE(ArchiveDir, std::string{"archive/"})

    COG_MAGIC_VALUE(IncludeDir, std::string{"include/"})
    
    COG_MAGIC_VALUE(DriversDir, std::string{"bin/"}); 
 
    NEW_COG_SIMPLE_SET(Dependencies, std::string)
  END_COG()  
}

