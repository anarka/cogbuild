#pragma once
#include <unordered_map>
#include <string>
#include <CogCore/File.h>

namespace Cog::Build
{
  class Template
  {
  public:
    Template(Filesystem::File sourceFile);

  private:
    std::unordered_map<std::string, std::string> myArgs{};
  };
}
