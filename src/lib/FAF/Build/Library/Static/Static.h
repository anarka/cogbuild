#pragma once
#include <functional>

#include <CogCore/Directory.h>
#include <CogCore/File.h>
#include <CogCore/Vector.h>

#include <CogBuild/BuildCog.h>

namespace Cog::Build
{

  class StaticLibrary
  {
  public:
    static StaticLibrary Build(const Filesystem::ExistingDirectory& libDir, const LanguageCog<BuildCog>& configFile);

    const Filesystem::File& Library() const;
    const Collections::Vector<Filesystem::File>& Includes() const
    {
      return m_includeFiles;
    }

  private:
    Filesystem::File m_archive{};
    Collections::Vector<Filesystem::File> m_objects{};
    Collections::Vector<Filesystem::File> m_includeFiles{};
  };
}
