#include <future>

#include <CogCore/Logger.h>
#include <CogCore/Process.h>
#include <CogCore/ParallelFor.h>
#include <CogBuild/BuildCog.h>
#include <CogBuild/Archiver.h>

#include <CogBuild/Static.h>

namespace Cog::Build
{

  StaticLibrary StaticLibrary::Build(const Filesystem::ExistingDirectory& libDir, const LanguageCog<BuildCog>& languageCog)
  {
    StaticLibrary newLib{};

    const auto sourceExt = languageCog.SourceExt();
    auto&& compiler = languageCog.Compiler();
    const auto interfaceExt = languageCog.InterfaceExt();

    newLib.m_includeFiles = libDir.CollectFilesByExt(interfaceExt);

    Logging::ColorLog("Staging headers in %s", Logging::Colors::CYAN, languageCog.LocalIncludeDir());

    for (auto&& header : newLib.m_includeFiles)
    {
      auto targetFile = header.LocatedAt(languageCog.LocalIncludeDir());

      header.DoIfNewer(targetFile, &Filesystem::File::EnsureCopyTo, header, targetFile);
    }

    newLib.m_archive = languageCog.ArchiveFile();

    const Archiver libArchiver{newLib.m_archive};

    const auto sources = libDir.CollectFilesByExt(sourceExt);

    Async::ParallelDispatcher<Filesystem::File>
    {
        sources, 
        Functional::OnThis(&Compiler::Compile, compiler),
        false
    }.Then(Functional::OnThis(&Archiver::Archive, libArchiver))
     .Wait("Object file failed to build: ", Functional::OnThis(&Collections::Vector<Filesystem::File>::EmplaceBack<const Filesystem::File&>, newLib.m_objects));

    compiler.WriteYCMConfig();
    
    return newLib;
  }

  const Filesystem::File& StaticLibrary::Library() const
  {
    return m_archive;
  }
}
