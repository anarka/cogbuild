#pragma once

#include <CogCore/File.h>
#include <CogCore/ConfigFile.h>
#include <fstream>
#include <string_view>
#include <unordered_map>
#include <cassert>
#include <CogCore/DynamicFunction.h>
#include <CogCore/Process.h>
#include <CogCore/RequiredDirectory.h>
#include <optional>

namespace Cog::Compile
{
  class PackageTree
  {
  public:
 /*   PackageTree(const std::vector<std::string>& package, size_t index = 0)
      : myPackage{package[index]}
    {
      assert(index < package.size() - 1);

      if (index == package.size() - 1)
      {
        myPackage = package[index];
      }
      else
      {
        if(!myChildPackages.contains(package[index]))
        {
          
        }
        else
        {
          myChildPackages.emplace(std::piecewise_construct, 
                                  std::forward_as_tuple(package[index]), 
                                  std::forward_as_tuple(package[index]));

        }
      }
    }
*/
  private:
    std::string myPackage{}; 
    std::vector<std::string> myCogs{};
//    std::unordered_map<std::string, PackageTree> myChildPackages{};
  };

  class Package
  {
  public:
    static constexpr std::string_view KEY = "module";

    Package() = default;

    Package(const std::string& package) :
      myPackagePath{Tokenize(package, '.')}
    {}

    const std::vector<std::string>& PackagePath()
    {
      return myPackagePath;
    }

  private:
    std::vector<std::string> myPackagePath{};
  };

  class Import
  {
  public:
    static constexpr std::string_view KEY = "import";
    Import(std::string importPath)
      : myFilePath{std::move(importPath)}
    {
      std::replace(myFilePath.begin(), myFilePath.end(), '.', '/');
      myFilePath += ".h";
    }

    void WriteTo(std::ostream& outFile)
    {
      outFile << "#include <" << myFilePath << ">\n";
    };
  private:
    std::string myFilePath{};
  };


  class LimitedWriter
  {
  public:
    LimitedWriter(size_t remainingWrites, std::string toWrite) :
      myRemainingWrites{remainingWrites},
      myWriteOut{std::move(toWrite)}
    {}

    const std::string& Write()
    {
      if (myRemainingWrites != 0 && myRemainingWrites-- >= 1)
      {
        return myWriteOut;
      }
      else
      {
        if (!myWriteOut.empty())
        {
          myWriteOut.clear();
        }

        return myWriteOut;
      }
    }
  private:
    size_t myRemainingWrites;
    std::string myWriteOut;
  };

  class TabPrinter
  {
  public:
    auto Increment()
    {
      ++myTabCount;
    }

    auto Decrement()
    {
      --myTabCount;
    }

    auto Write() const
    {
      std::string out{};
      for (size_t i = 0; i < myTabCount; ++i)
      {
        out += TabStr;
      }

      return out;
    }
  private:
    size_t myTabCount{};
    static constexpr auto TabStr = "  ";
  };

  template<class Guarded>
  class IncrementGuard
  {
  public:
    IncrementGuard(Guarded& toGuard) :
      myGuarded{toGuard}
    {
      myGuarded.Increment();
    }

    ~IncrementGuard()
    {
      myGuarded.Decrement();
    }
  private:
    Guarded& myGuarded;
  };

  class ScopeWriter
  {
  public:
    ScopeWriter(std::string start, std::string end, TabPrinter& tabber, std::ostream& outFile) :
      myScopeStartSymbol{std::move(start)},
      myScopeEndSymbol{std::move(end)},
      myTabPrinter{tabber},
      myOutFile{outFile}
    {
      myOutFile << myTabPrinter.Write() << myScopeStartSymbol;
      myTabPrinter.Increment();
    }

    ~ScopeWriter()
    {
      myTabPrinter.Decrement();
      myOutFile << myTabPrinter.Write() << myScopeEndSymbol;
    }

  private:
    std::string myScopeStartSymbol;
    std::string myScopeEndSymbol;
    TabPrinter& myTabPrinter;
    std::ostream& myOutFile;
  };


  class DynamicFromString
  {
  public:

  private:
    GENERIC_ERASER(FromString)
  };

  
  class Function
  {
  public:
    static constexpr std::string_view KEY = "function";
    static constexpr std::string_view END_KEY = "endFunc";

    Function(std::string signature, std::vector<std::string>&& functionBody) :
      myName{},
      myBody{std::move(functionBody)}
    {
      auto&& [predicate, args] = Split(signature, "<-", [](auto&& in) { return SplitString{in, ""};});
      auto&& [returnType, name] = Split(predicate, ':', [](auto&& in) { return SplitString{"auto", in}; });
      myName = std::move(name);
      myReturnType = std::move(returnType);
      auto&& argNamePairs = Tokenize(args, ',');
      for(auto&& pair : argNamePairs)
      {
        myArguments.emplace_back(Split(pair, "->", [](auto&& input) { return SplitString{input, ""}; }));
      } 
    }
    
    const std::string& Name() const
    {
      return myName;
    }

    std::string Body() const
    {
      std::stringstream out{};
      out << Signature() << "\n";
      {
        auto tabs = TabPrinter{};
        auto scope = ScopeWriter{"{\n", "}\n", tabs, out};
        for(auto&& line : myBody)
        {
          out << line << ";\n";
        }
      }
      return out.str();
    }

    std::string Signature(bool returnType = true, const std::vector<std::pair<std::string, std::string>>& extraArgs = {}) const
    {
      std::string out{};

      if (returnType)
      {
        out += myReturnType + " "; 
      }

      out += myName + "( ";
      
      for(auto&& [type, name] : extraArgs)
      {
        out += type + " " + name + ",";
      }
      
      for(auto&& arg : myArguments)
      {
        if (!arg.first.empty())
        {
          out += std::string{arg.first} + " " + arg.second + ",";
        }
      }
      out.back() = ')';
      return out;
    }

    std::string Decltype() const
    {
      std::string out{myName + "("};
      for(auto&& arg : myArguments)
      {
        out += "std::declval<" + std::string{arg.first} + ">(),";
      }

      std::replace(out.begin(), out.end(), '&', ' ');
      out.back() = ')';
      return out;
    }

    std::string DefaultCall(const std::vector<std::string>& extraArguments = {}) const
    {
      std::string out{myName + "( "};
      for(auto&& extraArg : extraArguments)
      {
        out += extraArg + ",";
      }

      for(auto&& arg : myArguments)
      {
        if(!arg.second.empty())
        {
          out += arg.second + ",";
        }
      }
      out.back() = ')';
      return out;
    }

   std::string FunctionPointerVariable(const std::vector<std::string>& extraArguments = {})  const
   {
     std::string out{};
     out += myReturnType + "(*" + myName + ")("; 
     
     for(auto&& arg : extraArguments)
     {
      out += std::string{arg} + ",";
     } 

     for(auto&& arg : myArguments)
     {
      if (!arg.first.empty())
      {
        out += std::string{arg.first} + ",";
      }
     }
     
     out.back() = ')'; 
     return out;
   }

  private:
    std::string myName{};
    std::string myReturnType{};
    std::vector<std::pair<std::string, std::string>> myArguments{};
    std::vector<std::string> myBody;
  };
  
  class Interface
  {
  public:
    static constexpr std::string_view KEY = "export";
    static constexpr std::string_view END_KEY = "endFunc";

    Interface(const std::string& signature, std::vector<std::string>&& functionBody) :
      myFunction{signature, std::move(functionBody)}
    {}

   const Function& AsFunction() const
   {
     return myFunction;
   } 


  private:
    Function myFunction;
  };

  class Type
  {
  public:
    static constexpr std::string_view KEY = "type";
    static constexpr std::string_view END_KEY = "endType";
    Type(std::string typeName, std::string valueType, std::string defaultValue = {}) :
      myTypeName{std::move(typeName)},
      myValueType{std::move(valueType)},
      myDefaultValue{std::move(defaultValue)}
    {}
   
    std::string Declaration(std::string varName) const
    {
      return std::string{myValueType + " " + varName + "{" + myDefaultValue + "};"};
    }

    std::string AliasAs(std::string alias) const
    {
      return std::string{"using " + alias + " = " + myTypeName + ";"};
    }

    std::string Definition() const
    {
      TabPrinter tabs{};
      std::stringstream out{"struct " + myTypeName + "\n"};
      {
        auto definitonScope = ScopeWriter("{\n", "};\n", tabs, out);
        out << tabs.Write() << "using ValueType = " << myValueType << ";\n";
        out << tabs.Write() << "inline static const ValueType Default = " << myDefaultValue << ";\n";
      }

      return out.str();
    }

    const std::string& TypeName = myTypeName;
    const std::string& ValueType = myValueType;
    const std::string& DefaultValue = myDefaultValue;

  private:
    std::string myTypeName{};
    std::string myValueType{};
    std::string myDefaultValue{};
  };

  class Member
  {
  public:
    static constexpr std::string_view KEY = "member";
    static constexpr std::string_view END_KEY = "endMember";
    Member(const std::string& typeName, const std::string& valueType, const std::string& defaultValue = {}) :
      myType{typeName, valueType, defaultValue},
      myVariableName{"my" + typeName}
    {}

  private:
    Type myType;
    const std::string myVariableName;
  };

  class CogTranslator
  {
  public:
    CogTranslator(const Filesystem::File& inFile, const Filesystem::File& outDir = Filesystem::File{"./"}) :
      myBuiltFrom{inFile},
      myCogName{inFile.Fullname()},
      myOutFile{inFile.WithExt(".cpp").LocatedAt(outDir)}
    {
      std::replace(myCogName.begin(), myCogName.end(), '.', '_');
      std::replace(myCogName.begin(), myCogName.end(), '+', 'p');
      if (myOutFile.Timestamp() > inFile.Timestamp())
      {
        return;
      }

      Logging::ErrorLog("Starting to build file %s", inFile);
      std::ifstream readFile{inFile.Path()};
      std::string curLine{};
      while(std::getline(readFile, curLine))
      {
        if(curLine.empty()) continue;

        if (auto&& [key, value] = Split(curLine, ' ', [](auto&&) {return SplitString{};}); key == Import::KEY)
        {
          myImports.emplace_back(std::string{value});
        }
        else if (key == Package::KEY)
        {
          myPackage = Package{std::string{value}};
        }
        else if (key == Function::KEY || key == Interface::KEY)
        {
          auto curKey = std::string{key};
          std::string funcSig{value}; 
          std::vector<std::string> funcBody{};
          for(std::getline(readFile,curLine);curLine != Function::END_KEY && readFile;std::getline(readFile,curLine))
          {
            funcBody.emplace_back(curLine);
          }

          if(curKey == Function::KEY)
          {
            myFunctions.emplace_back(std::move(funcSig), std::move(funcBody));
          }
          else
          {
            myInterface.emplace_back(std::move(funcSig), std::move(funcBody));
          }
        }
        else
        {
          Logging::ErrorLog("Line not recognized %s", curLine);
        }
      }

      std::ofstream outSource{myOutFile.Path()};

      for(auto&& import : myImports)
      {
        import.WriteTo(outSource);
      }

      WriteNamespace(outSource, &CogTranslator::WriteClass);
     
      WriteLn(outSource, myNamespace + "::VTable exports{};");
      
      outSource << "using namespace Cog;\n";


      for(auto&& function : myFunctions)
      {
        outSource << "extern \"C\" decltype(" << myNamespace << "::" << myCogName << "::" << function.Decltype() << ") " << function.Signature(false);
        auto scope = ScopeWriter("{\n", "}\n", myTabs, outSource);
        outSource << myTabs.Write() << "return " << myNamespace << "::" << myCogName << "::" << function.DefaultCall() << ";\n";
      }

      std::ofstream outInterface{myOutFile.WithExt(".h").Path()};
      outInterface << "#pragma once\n";
      WriteLn(outInterface, "#include <cstddef>");
      WriteLn(outInterface, "#include <CogBuild/ClassWriter.h>");
      WriteNamespace(outInterface, &CogTranslator::WriteInterface);
    }

    template<typename Callback>
    void WriteNamespace(std::ofstream& outSource, Callback&& thenWrite)
    {
      outSource << "\nnamespace ";

      LimitedWriter seperatorWriter{myPackage.PackagePath().size() - 1, "::"};
      for(auto&& curNamespace : myPackage.PackagePath())
      {
        myNamespace += curNamespace + seperatorWriter.Write();
      }

      Write(outSource, myNamespace);
      outSource << "\n";

      auto namespaceScope = ScopeWriter("{\n", "}\n", myTabs, outSource);
    
      Functional::Call(thenWrite, this, outSource);
    }

    void WriteClass(std::ofstream& outSource)
    {
      WriteLn(outSource, "struct " + myCogName);
      {
        auto namespaceScope = ScopeWriter("{\n", "};\n", myTabs, outSource);
    
        for(auto&& func : myFunctions)
        {
          WriteLn(outSource, "static " + func.Body());
        }

        for(auto&& func : myInterface)
        {
          WriteLn(outSource, func.AsFunction().Body());
        }
      }

      WriteLn(outSource, "using TicketType = size_t;");
      
      WriteLn(outSource, "struct VTableImpls");
      {
        auto vTableScope = ScopeWriter("{\n", "};\n", myTabs, outSource);
        for(auto&& event : myInterface)
        {
          WriteLn(outSource, "static " + event.AsFunction().Signature(true, {{"TicketType", "ticket"}}));
          {
            auto implScope = ScopeWriter("{\n", "}\n", myTabs, outSource);
            WriteLn(outSource, "return ourTickets.at(ticket)." + event.AsFunction().DefaultCall() + ";");
          }

          WriteLn(outSource, "inline static std::unordered_map<size_t, " + myCogName + "> ourTickets{};");
        }

        WriteLn(outSource, "static TicketType GetNew()");
        {
          auto implScope = ScopeWriter("{\n", "}\n", myTabs, outSource);
          WriteLn(outSource, "ourTickets[0];return 0;");
        }
        WriteLn(outSource, "static void ReleaseTicket(TicketType) {}");
      }

      WriteLn(outSource, "");
      
      WriteLn(outSource, "struct VTable");
      {
        auto vTableScope = ScopeWriter("{\n", "};\n", myTabs, outSource);
        for(auto&& event : myInterface)
        {
          WriteLn(outSource, event.AsFunction().FunctionPointerVariable({"TicketType"}) + " = &VTableImpls::" + event.AsFunction().Name() + ";");
        }

        WriteLn(outSource, "TicketType(*GetNew)() = &VTableImpls::GetNew;");
        WriteLn(outSource, "void(*ReleaseTicket)(TicketType) = &VTableImpls::ReleaseTicket;");
      }

      WriteLn(outSource, "");

    }

    void WriteInterface(std::ofstream& outFile)
    {

      WriteLn(outFile, "struct " + myCogName);
      auto classScope = ScopeWriter("{\n", "};\n", myTabs, outFile);
      for (auto&& event : myInterface)
      {
        WriteLn(outFile, event.AsFunction().Signature());
        auto functionScope = ScopeWriter("{\n", "}\n", myTabs, outFile);
        WriteLn(outFile, "return myLoadedFunctions()." + event.AsFunction().DefaultCall({"myTicket"}) + ";"); 
      }

      WriteLn(outFile, "~" + myCogName + "()");
      {
        auto functionScope = ScopeWriter("{\n", "}\n", myTabs, outFile);
        WriteLn(outFile, "myLoadedFunctions().ReleaseTicket(myTicket);");
      }

      myTabs.Decrement();

      WriteLn(outFile, "private:");
      
      myTabs.Increment();

      WriteLn(outFile, "using TicketType = size_t;");
      WriteLn(outFile, "struct CurrentFunctions");
      {
        auto hotLoadFunctionsScope = ScopeWriter("{\n", "};\n", myTabs, outFile);
        WriteLn(outFile, "struct VTable");
        {
          auto vTableScope = ScopeWriter("{\n", "};\n", myTabs, outFile);
          for(auto&& event : myInterface)
          {
            WriteLn(outFile, event.AsFunction().FunctionPointerVariable({"TicketType"}) + ";");
          }

          WriteLn(outFile, "TicketType(*GetNew)();");
          WriteLn(outFile, "void(*ReleaseTicket)(TicketType);");
        }

        WriteLn(outFile, "");
        
        WriteLn(outFile, "const VTable& operator()() const");
        {
          auto accessorScope = ScopeWriter("{\n", "}\n", myTabs, outFile);
          WriteLn(outFile, "if (!myLoadedCog.Current())");
          {  
            auto ifScope = ScopeWriter("{\n", "}\n", myTabs, outFile);
            WriteLn(outFile, "myVTable = reinterpret_cast<VTable*>(myLoadedCog.Cog().Load(\"exports\"));"); 
          }
          WriteLn(outFile, "return *myVTable;");
        }

        WriteLn(outFile, "");
        WriteLn(outFile, "Compile::JITCog myLoadedCog{Filesystem::File{\"" + myBuiltFrom.Path() + "\"}, Filesystem::File{\"" + myOutFile.Location().Path()  + "/\"}};");
        WriteLn(outFile, "mutable VTable* myVTable{reinterpret_cast<VTable*>(myLoadedCog.Cog().Load(\"exports\"))};");
      }
      
      WriteLn(outFile, "");
      WriteLn(outFile, "CurrentFunctions myLoadedFunctions{};");
      WriteLn(outFile, "TicketType myTicket{myLoadedFunctions().GetNew()};");
    }

    const Filesystem::File& Outfile()
    {
      return myOutFile;
    }

  private:
    Filesystem::File myBuiltFrom;
    std::string myCogName;
    Package myPackage;
    std::vector<Import> myImports{};
    TabPrinter myTabs{};
    std::vector<Function> myFunctions{};
    std::vector<Interface> myInterface{};
    std::string myNamespace{};
    Filesystem::File myOutFile;

    void Write(std::ofstream& outFile, const std::string& outString)
    {
      outFile << myTabs.Write() << outString; 
    }

    void WriteLn(std::ofstream& outFile, const std::string& outString)
    {
      for(auto&& line : Tokenize(outString, '\n'))
      {
        Write(outFile, line);
        outFile << "\n";
      }
    }
  };

  class JITCog
  {
    struct BadBuilder;
  public:
    JITCog(const Filesystem::File& cogPath, const Filesystem::File& outputDir = Filesystem::File{"./"}) :
      myOutputDir{outputDir},
      myCogPath{cogPath},
      myCog{CogTranslator{cogPath, outputDir}.Outfile(), outputDir}
    {}

    const auto& Cog() const
    {
      if (myCog.Timestamp() < myCogPath.Timestamp())
      {
        myCog = Functional::JITObject<BadBuilder>{CogTranslator{myCogPath, myOutputDir}.Outfile(), myOutputDir};
      }

      return myCog.myTargetObject;
    }

    bool Current() const
    {
      return myCog.Timestamp() > myCogPath.Timestamp();
    }

  private:
    Filesystem::File myOutputDir;
    Filesystem::File myCogPath;
    mutable Functional::JITObject<BadBuilder> myCog;
    struct BadBuilder
    {
      static Filesystem::File Build(const Filesystem::File& sourceFile, const Filesystem::File& outputDir)
      {
        Filesystem::File outFile = sourceFile.WithExt(".so").LocatedAt(outputDir);

        if (sourceFile.Timestamp() > outFile.Timestamp())
        {
          System::Process::CreateAndWait("g++-10", {"-I/home/arkos/.local/share/Cog/include/CogCore/c++", "-I./build/include/c++", sourceFile.Path(), "-g", "-shared", "-rdynamic", "-Wl,-soname,test.so.1", "-o" + outFile.Path(), "-std=c++2a", "-fPIC", "-lstdc++fs", "/home/arkos/.local/share/Cog/lib/libCogCore.a"});
        }

        return outFile;
      }
    };

  };
  
  COG(Parser)
    NEW_COG_SIMPLE(Key, "")

    NEW_COG_SIMPLE(WriteFormat, "")

    //NEW_COG_SIMPLE_VAR(ValuePosition, 0llu)

    NEW_COG_VARIABLE(AdditionalArgs, std::vector<std::string>{})
      CONVERT_FUNCTION()
      {
        return Tokenize(inVal, ',');
      }
    END_COG_VARIABLE(AdditionalArgs)

    NEW_COG_SIMPLE_VAR(OutputDir, Filesystem::RequiredDirectory{"build/artifacts/"})

    std::string OnParse(const std::string&)
    {
      return {};
    }

    auto OutString()
    {
      auto out = WriteFormat();
      //auto&& formatter = Logging::Formatter::Get();

      for (size_t i = 0; i < AdditionalArgs().size(); ++i)
      {
      //  if(i == ValuePosition())
        {
          //out = formatter.Format(out, 
        }
      }

      //return Logging::Formatter::Get().Format(WriteFormat(), 
    }

    NEW_COG_DERIVED(JitMe, std::optional<JITCog>)
    {
      toFinalize.emplace(JITCog{My, My.OutputDir()});
    }
    END_PRIVATE_COG_VARIABLE(JitMe)
  END_COG()
}
