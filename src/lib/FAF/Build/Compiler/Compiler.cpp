#include <CogCore/Directory.h>

#include <CogBuild/Compiler.h>
#include <CogBuild/BuildCog.h>

namespace Cog::Build
{

  Filesystem::File Compiler::NewestLibrary() const
  {
    Filesystem::File newest{};
    for(auto&& lib : m_staticLibs)
    {
      newest = lib.Timestamp() > newest.Timestamp() ? lib : newest;
    }

    return newest;
  }

  void Compiler::AddIncludeDir(const Filesystem::File& dir)
  {
    m_includeDirs.emplace(dir);
  }

  void Compiler::AddStaticLib(const Filesystem::File& lib)
  {
    m_staticLibs.emplace(lib);
  }

  void Compiler::AddLinkerDepends(const std::string_view& depend)
  {
    m_linkedLibs.emplace(depend);
  }

  template<typename Logger>
  void Compiler::StateDump(std::optional<std::reference_wrapper<Logger>> logger) const
  {
    auto&& scopedLogger = logger ? logger->get().Log("DUMPING COMPILER BUILD STATE\n") : Logging::ErrorLog("DUMPING COMPILER BUILD STATE\n");
    scopedLogger.LogColor(Logging::Colors::MAGENTA, "\tINCLUDE DIRS");

    auto&& LogState = [&scopedLogger](auto&& state)
    {
      scopedLogger.LogColor(Logging::Colors::CYAN, "\t\t- %s", std::forward<decltype(state)>(state));
    };

    for(auto&& dir : m_includeDirs)
    {
      LogState(dir.Path());
    }
 
    scopedLogger.LogColor(Logging::Colors::MAGENTA, "\tSTATIC LIBS");

    for(auto&& lib : m_staticLibs)
    {
      LogState(lib.Path());
    }

    scopedLogger.LogColor(Logging::Colors::MAGENTA, "\tFLAGS");

    for(auto&& flag : m_flags)
    {
      LogState(flag);
    }

    scopedLogger.LogColor(Logging::Colors::MAGENTA, "\tLINKED LIBS");

    for(auto&& lib : m_linkedLibs)
    {
      LogState(lib);
    }
  }

  void Compiler::WriteYCMConfig() const
  {
    std::ofstream ycm_file{".ycm_extra_conf.py"};
    ycm_file << "def Settings( **kwargs ):\n    return {\n        \'flags\' : [ \'-x\', \'c++\' ";

    for (auto&& flag : m_flags)
    {
      ycm_file << ", \'" << flag << "\' ";
    }
     
    for (auto&& includeDir : m_includeDirs)
    {
      ycm_file << ", \'-I" << includeDir.Path() << "\' ";
    }

    ycm_file << "]\n    }\n";
  }

  Filesystem::File Compiler::Compile(const Filesystem::File& sourceFile, bool binary) const
  {
    const auto ext = binary ? "" : ".o"; //No Ext for executable files
    const auto locatedAt = binary ? m_binaryOutputDir : m_archiveOutputDir;
    const auto objectFile = sourceFile.WithExt(ext).LocatedAt(locatedAt);
    const auto includesFile = sourceFile.WithExt(".includes").LocatedAt(m_archiveOutputDir);
    Collections::Vector<std::string> args{};

    args.EmplaceBack("-MM");
    args.ApplyAndAppend(m_includeDirs, [](auto&& dir) { return "-I" + dir.Path(); });

    args.EmplaceBack(sourceFile.Path());
    args.EmplaceBack("-o" + includesFile.Path());

    auto dependencyGraphGenerator = System::Process(std::string{m_compiler}, args.AsVector());

    Filesystem::File newest{};

    if (dependencyGraphGenerator.ReturnValue() != 0)
    {
      Logging::ErrorLog("Dependency generation failed! Rebuild might miss changes");
    }
    else
    {
      std::ifstream readDepends{includesFile.Path()};
      std::string objectName{};
      readDepends >> objectName;
      
      std::string curFile{};

      while(readDepends >> curFile >> std::ws)
      {
        if (curFile != "\\")
        {
          if(newest.Timestamp() < Filesystem::File{curFile}.Timestamp())
          {
            newest = Filesystem::File{curFile}; 
          }
        }
      }
    }
    
    args.pop_back();
    
    const auto newestDependency = [this](const auto& other)
    {
      const auto newestLib = this->NewestLibrary();
      return other.Timestamp() < newestLib.Timestamp() ? newestLib : other;
    }(newest.Timestamp() > sourceFile.Timestamp() ? newest : sourceFile);

    newestDependency.DoIfNewer(objectFile, [this, &args, &objectFile, binary, &sourceFile]()
    {
      Logging::ColorLog("Building file %s from source %s", Logging::Colors::MAGENTA, objectFile.Fullname(), sourceFile.Fullname());
       
      if(!binary)
      {
        args[0] = "-c"; 
      }
      else
      {
        args.erase(args.begin());
      }

      args.ApplyAndAppend(m_staticLibs, [](auto&& lib) { return lib.Path(); });

      args.Append(m_flags);
     
      args.Append(m_linkedLibs);

      args.emplace_back("-o" + objectFile.Path());

      auto compiler = System::Process(std::string{m_compiler}, args.AsVector());

      if(compiler.ReturnValue() != 0)
      {
        auto&& scopedLogger = Logging::ErrorLog("Failed to build %s", objectFile.Fullname());
      
        auto loggerArg = std::optional(std::ref(scopedLogger));

        StateDump(loggerArg);

#ifdef COMPILER_FLAG_DEBUG
        scopedLogger.Log("Compiler recieved flags:");
        
        for(auto&& arg : args)
        {
          scopedLogger.Log(arg);
        }
#endif

        compiler.LogErrors(loggerArg);

        throw std::runtime_error("Failed to compile " + sourceFile.Fullname());
      }

      Logging::ColorLog("Successfully built %s!", Logging::Colors::GREEN, objectFile.Fullname());
    }).ElseIf(Functional::Identor(true), [&objectFile]()
    {
        Logging::ColorLog("%s current, no need to recompile!", Logging::Colors::GREEN, objectFile.Fullname());
    });

    return objectFile;
 
  }
}
