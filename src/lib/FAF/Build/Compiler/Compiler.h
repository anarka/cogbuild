#pragma once

#include <fstream>
#include <functional>
#include <string>
#include <unordered_set>

#include <CogCore/Logger.h>
#include <CogCore/Process.h>
#include <CogCore/File.h>
#include <CogCore/RequiredDirectory.h>

namespace Cog::Build
{
  class BuildCog;
  class Compiler
  {
  public:
    Compiler(const std::string_view& compilerExe)
      : m_compiler{std::string{compilerExe}}
    { 
    };

    Compiler()
    {
    }

    void AddIncludeDir(const Filesystem::File& dir);

    void AddStaticLib(const Filesystem::File& lib);
    
    void AddLinkerDepends(const std::string_view& depend);

    void AddFlag(const std::string_view& flag)
    {
      m_flags.emplace(flag);
    }

    void SetBuildDir(const std::string_view& buildDir)
    {
      m_buildDir = buildDir;
    }

    Filesystem::File NewestLibrary() const;

    Filesystem::File Compile(const Filesystem::File& sourceFile, bool binary = false) const;

    void WriteYCMConfig() const;

    void SetBinaryOutputDir(const Filesystem::RequiredDirectory& outputDir)
    {
      m_binaryOutputDir = outputDir;
    }

    void SetArchiveOutputDir(const Filesystem::RequiredDirectory& outputDir)
    {
      m_archiveOutputDir = outputDir;
    }
  private:

    std::string_view m_buildDir{};


    std::string m_compiler{};
    Filesystem::RequiredDirectory m_archiveOutputDir{};
    Filesystem::RequiredDirectory m_binaryOutputDir{};
    std::unordered_set<Filesystem::File, Filesystem::File::Hashor> m_includeDirs{};
    std::unordered_set<Filesystem::File, Filesystem::File::Hashor> m_staticLibs{};
    std::unordered_set<std::string> m_linkedLibs{};
    std::unordered_set<std::string_view> m_flags{};

    template<typename Logger>
    void StateDump(std::optional<std::reference_wrapper<Logger>> = std::nullopt) const;
  };
}
