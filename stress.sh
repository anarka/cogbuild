for counter in {1..100}
do
  echo $counter
  valgrind ./build/bin/CogBuild
  if [ $? -ne 0 ]; then
    break
  fi
  valgrind ./build/bin/CogBuild rebuild
  if [ $? -ne 0 ]; then
    break
  fi
  valgrind ./build/bin/CogBuild
  if [ $? -ne 0 ]; then
    break
  fi
done
