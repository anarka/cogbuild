#!/bin/bash

shopt -s globstar

rm -rf bootstrap
mkdir bootstrap
cd bootstrap

git clone https://gitlab.com/anarka/cogcore
cd cogcore
mkdir -p build/include/CogCore
cp src/**/*.h build/include/CogCore
g++ -c -Ibuild/include  src/lib/**/*.cpp -std=c++2a -lpthread -lstdc++fs

cd ../..

rm -r build/

mkdir -p build/include/CogBuild
cp src/**/*.h build/include/CogBuild

g++ -c -Ibuild/include -Ibootstrap/cogcore/build/include src/lib/**/*.cpp -std=c++2a -lpthread -lstdc++fs -O3
g++ -Ibuild/include -Ibootstrap/cogcore/build/include src/bin/Drivers/CogBuild.cpp *.o bootstrap/cogcore/*.o -std=c++2a -oCogBootstrap -lpthread -lstdc++fs -O3
g++ -Ibuild/include -Ibootstrap/cogcore/build/include src/bin/Drivers/FogManage.cpp *.o bootstrap/cogcore/*.o -std=c++2a -oCogBootstrap-Manager -lpthread -lstdc++fs -O3

rm *.o

cd bootstrap/cogcore

rm *.o

../../CogBootstrap rebuild
../../CogBootstrap-Manager install

cd ../..

CogBootstrap rebuild
CogBottstrap-Manager install
